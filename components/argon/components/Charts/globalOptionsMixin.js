import Chart from 'chart.js';
import { initGlobalOptions } from "../Charts/config";
export default {
  mounted() {
    initGlobalOptions(Chart);
  }
}
