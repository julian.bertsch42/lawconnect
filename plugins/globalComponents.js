//register components
import Badge from "../components/argon/components/Badge";
import BaseAlert from "../components/argon/components/BaseAlert";
import BaseButton from "../components/argon/components/BaseButton";
import BaseCheckbox from "../components/argon/components/BaseCheckbox";
import BaseInput from "../components/argon/components/BaseInput";
import BaseDropdown from "../components/argon/components/BaseDropdown";
import BaseNav from "../components/argon/components/BaseNav";
import BasePagination from "../components/argon/components/BasePagination";
import BaseProgress from "../components/argon/components/BaseProgress";
import BaseRadio from "../components/argon/components/BaseRadio";
import BaseSlider from "../components/argon/components/BaseSlider";
import BaseSwitch from "../components/argon/components/BaseSwitch";
import BaseTable from "../components/argon/components/BaseTable";
import BaseHeader from "../components/argon/components/BaseHeader";
import Card from "../components/argon/components/Card";
import StatsCard from "../components/argon/components/StatsCard";
import Modal from "../components/argon/components/Modal";
import TabPane from "../components/argon/components/Tabs/TabPane";
import Tabs from "../components/argon/components/Tabs/Tabs";

Vue.component(Badge.name, Badge);
Vue.component(BaseAlert.name, BaseAlert);
Vue.component(BaseButton.name, BaseButton);
Vue.component(BaseInput.name, BaseInput);
Vue.component(BaseNav.name, BaseNav);
Vue.component(BaseDropdown.name, BaseDropdown);
Vue.component(BaseCheckbox.name, BaseCheckbox);
Vue.component(BasePagination.name, BasePagination);
Vue.component(BaseProgress.name, BaseProgress);
Vue.component(BaseRadio.name, BaseRadio);
Vue.component(BaseSlider.name, BaseSlider);
Vue.component(BaseSwitch.name, BaseSwitch);
Vue.component(BaseTable.name, BaseTable);
Vue.component(BaseHeader.name, BaseHeader);
Vue.component(Card.name, Card);
Vue.component(StatsCard.name, StatsCard);
Vue.component(Modal.name, Modal);
Vue.component(TabPane.name, TabPane);
Vue.component(Tabs.name, Tabs);

export default {

};
