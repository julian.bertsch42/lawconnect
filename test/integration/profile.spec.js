import { shallowMount, mount, createLocalVue, RouterLinkStub } from '@vue/test-utils';
import profile from '@/pages/dashboard/Profile.vue';
import axios from 'axios';
import moxios from 'moxios';
import flushPromises from 'flush-promises'
import Vue from 'vue'
import Notifications from 'vue-notification'
import frontendStub from '~/components/layouts/frontend.vue'
import stepsStub from '~/components/matchingsteps.vue'
Vue.use(Notifications)
Vue.use(axios)
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)
import $ from 'jquery';
global.$ = global.jQuery = $;


describe('matchingflow', () => {
    beforeEach(function () {
        // import and pass your custom axios instance to this method
        moxios.install(axios)
        Vue.prototype.$axios = axios
        Vue.prototype.$auth = {}
        Vue.prototype.$auth.user = {"email":"julian.bertsch91@gmail.com","profileType":0,"createdAt":"2019-12-05T12:25:01.267Z","contacts":[{"_id":"5de8f9b4a689e44766ac935a","name":"Julian Bertsch","email":"julian.bertsch42@gmail.com","phone":"+10705112467","phase":0},{"_id":"5de8f9dda689e44766ac935b","name":"Christopher","email":"christopher.roeskes@code.berlin","phone":"+10705112467","phase":5}],"city":"berlin","description":"asd","name":"Julian Bertsch","profession":"Traffic Law"}
    })

    afterEach(function () {
        // import and pass your custom axios instance to this method
        moxios.uninstall()
    })

    test('test if the name is shown in profile', async () => {

        const wrapper = mount(profile, {
            Vue,
            stubs: {
                fontAwesomeIcon: true,
                baseDropdown: true,
                card: true,
                baseButton: true,
                NuxtLink: RouterLinkStub
            }
        })

        expect(wrapper.html()).toContain("Julian Bertsch")

    })

})
