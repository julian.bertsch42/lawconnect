import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils';
import Register from '@/pages/authentication/register.vue'
import axios from 'axios';
import moxios from 'moxios';
import flushPromises from 'flush-promises'
import Vue from 'vue'
import Notifications from 'vue-notification'
import Vuelidate from 'vuelidate'

Vue.use(Vuelidate)
Vue.use(Notifications)


describe('Register', () => {
    beforeEach(function () {
        // import and pass your custom axios instance to this method
        moxios.install(axios)
    })

    afterEach(function () {
        // import and pass your custom axios instance to this method
        moxios.uninstall()
    })

    test('has a email field', () => {
        const wrapper = shallowMount(Register, {
            Vue,
            stubs: {
                NuxtLink: RouterLinkStub
            }
        })

        expect(wrapper.contains('input[name=email]')).toBeTruthy()
    })

    test('has a password field', () => {
        const wrapper = shallowMount(Register, {
            Vue,
            stubs: {
                NuxtLink: RouterLinkStub
            }
        })

        expect(wrapper.contains('input[name=password]')).toBeTruthy()
    })

    test('has a submit button', () => {
        const wrapper = shallowMount(Register, {
            Vue,
            stubs: {
                NuxtLink: RouterLinkStub
            }
        })

        expect(wrapper.contains('button#registerSubmit')).toBeTruthy()
    })


    test('test to register successfully', async () => {
        moxios.stubRequest(process.env.BACKEND_API+"/user/lawyer/signup", {
            status: 200,
            response: {
                "message": "Lawyer for logging in was added successfully!"
            }
        });

        const wrapper = shallowMount(Register, {
            Vue,
            stubs: {
                NuxtLink: RouterLinkStub
            }
        })

        // mock login function
        wrapper.vm.login = function() {  }

        //fill form models
        wrapper.setData(
            {
                form: {
                    loading: false,
                    name: {
                        val: 'Lorem Ipsum',
                        msg: '',
                        valid: true
                    },
                    email: {
                        val: 'lorem@ipsum.de',
                        msg: '',
                        valid: true
                    },
                    password: {
                        val: 'lorem',
                        msg: '',
                        valid: true
                    },
                    success: false,
                    sent: false
                }
            }
        )

        wrapper.vm.$axios = axios

        wrapper.find('button#registerSubmit').trigger('click')

        await flushPromises()

        expect(wrapper.contains('div.alert-success')).toBe(true)
        // do we see a success msg
        // expect(wrapper.contains('Successfully signed in!')).toBe(true)
    })

    test('register with error', async () => {

        const errResp = {
            status: 500,
            response: {
                    "errors": {
                        "email": {
                            "message": "Error, expected `email` to be unique. Value: `neueres3@mail.com`",
                            "name": "ValidatorError",
                            "properties": {
                                "message": "Error, expected `email` to be unique. Value: `neueres3@mail.com`",
                                "type": "unique",
                                "path": "email",
                                "value": "neueres3@mail.com"
                            },
                            "kind": "unique",
                            "path": "email",
                            "value": "neueres3@mail.com"
                        }
                    },
                    "_message": "LawyerLogin validation failed",
                    "message": "LawyerLogin validation failed: email: Error, expected `email` to be unique. Value: `neueres3@mail.com`",
                    "name": "ValidationError"
                },
        };

        moxios.stubRequest(process.env.BACKEND_API+"/user/lawyer/signup", errResp);
        const wrapper = shallowMount(Register, {
            stubs: {
                NuxtLink: RouterLinkStub
            },
        })

        //fill form models
        wrapper.setData(
            {
                form: {
                    loading: false,
                    name: {
                        val: 'Lorem',
                        msg: '',
                        valid: true
                    },
                    email: {
                        val: 'neueres3@mail.com',
                        msg: '',
                        valid: true
                    },
                    password: {
                        val: 'lorem',
                        msg: '',
                        valid: true
                    },
                    success: false,
                    sent: false
                }
            }
        )

        wrapper.vm.$axios = axios


        wrapper.find('button#registerSubmit').trigger('click')
        await flushPromises()

        // do we see a error alert
        expect(wrapper.contains('div.has-danger')).toBe(true)

        // do we see the error msg
        expect(wrapper.html()).toContain('Error, expected `email` to be unique. Value: `neueres3@mail.com')
    })
})

