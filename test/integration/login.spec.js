import { shallowMount, mount, createLocalVue, RouterLinkStub } from '@vue/test-utils';
import Login from '@/pages/authentication/login.vue';
import axios from 'axios';
import moxios from 'moxios';
import flushPromises from 'flush-promises'
import Vue from 'vue'
import Notifications from 'vue-notification'

Vue.use(Notifications)

describe('Login', () => {
    beforeEach(function () {
        // import and pass your custom axios instance to this method
        moxios.install(axios)
    })

    afterEach(function () {
        // import and pass your custom axios instance to this method
        moxios.uninstall()
    })

    test('has a email field', () => {
        const wrapper = mount(Login, {
            Vue,
            stubs: {
                NuxtLink: RouterLinkStub
            }
        })

        expect(wrapper.contains('input[name=email]')).toBeTruthy()
    })

    test('has a password field', () => {
        const wrapper = mount(Login, {
            Vue,
            stubs: {
                NuxtLink: RouterLinkStub
            }
        })

        expect(wrapper.contains('input[name=password]')).toBeTruthy()
    })

    test('has a submit button', () => {
        const wrapper = mount(Login, {
            Vue,
            stubs: {
                NuxtLink: RouterLinkStub
            }
        })

        expect(wrapper.contains('button#loginSubmit')).toBeTruthy()
    })

    test('click on submit', async () => {
        const errResp = {
            status: 200,
            response: {
                "lawyerLoginId": "5d8ca4d92ff7791dcfbbdd5e",
                    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsYXd5ZXJMb2dpbklkIjoiNWQ4Y2E0ZDkyZmY3NzkxZGNmYmJkZDVlIiwiaWF0IjoxNTY5NzkxMjA5LCJleHAiOjE1Njk4MzQ0MDl9.u2yrjMYRMZYTl7JkocMm7XmFzet6L2qmr2WUMPEVDF4"
            }
        };

        moxios.stubRequest(process.env.BACKEND_API+"/user/lawyer/login", errResp);
        const wrapper = mount(Login, {
            Vue,
            stubs: {
                NuxtLink: RouterLinkStub
            },
        })

        wrapper.vm.$axios = axios
        wrapper.find('button#loginSubmit').trigger('click')
        await flushPromises()


        expect(wrapper.vm.form.sent).toBe(true)
    })
})
