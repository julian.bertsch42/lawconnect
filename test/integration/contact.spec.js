import { shallowMount, mount, createLocalVue, RouterLinkStub } from '@vue/test-utils';
import contact from '@/pages/dashboard/Contact/_id.vue';
import axios from 'axios';
import moxios from 'moxios';
import flushPromises from 'flush-promises'
import Vue from 'vue'
import Notifications from 'vue-notification'


describe('contact', () => {
    beforeEach(function () {
        // import and pass your custom axios instance to this method
        moxios.install(axios)
        Vue.prototype.$axios = axios
        // mock auth data
        Vue.prototype.$auth = {}
        Vue.prototype.$auth.user = {"email":"julian.bertsch91@gmail.com","profileType":0,"createdAt":"2019-12-05T12:25:01.267Z","contacts":[{"_id":"5de8f9b4a689e44766ac935a","name":"Julian Bertsch","email":"julian.bertsch42@gmail.com","phone":"+10705112467","phase":0},{"_id":"5de8f9dda689e44766ac935b","name":"Christopher","email":"christopher.roeskes@code.berlin","phone":"+10705112467","phase":5}],"city":"berlin","description":"asd","name":"Julian Bertsch","profession":"Traffic Law"}
        // mock route data
        Vue.prototype.$route = {}
        Vue.prototype.$route.params = {}
        Vue.prototype.$route.params.id = "5de8f9b4a689e44766ac935a"
    })

    afterEach(function () {
        // import and pass your custom axios instance to this method
        moxios.uninstall()
    })

    test('test if the email address is shown in contacts', async () => {

        const wrapper = mount(contact,  {
            Vue,
            stubs: {
                fontAwesomeIcon: true,
                card: true,
                baseButton: true,
                NuxtLink: RouterLinkStub
            }
        })
        await flushPromises()


        expect(wrapper.html()).toContain("julian.bertsch42@gmail.com")

    })

    test('test if the name is shown in contacts', async () => {

        const wrapper = mount(contact,  {
            Vue,
            stubs: {
                fontAwesomeIcon: true,
                card: true,
                baseButton: true,
                NuxtLink: RouterLinkStub
            }
        })
        await flushPromises()


        expect(wrapper.html()).toContain("Julian Bertsch")

    })

})
