import { mount } from '@vue/test-utils'
import testcomponent from '@/components/testcomponent.vue'
jest.mock('axios')

describe('testcomponent', () => {
    test('does it see array generated', () => {
        const wrapper = mount(testcomponent)
        expect(wrapper.html()).toContain('first one')
    })
})
