const host = process.env.FRONTENDE2E
const matchingflow_test = process.env.MATCHINGFLOW_TEST

module.exports = {

    'Test title' : function (browser) {
        browser
            .url(host)
            .waitForElementVisible('body')
            .assert.titleContains('lawconnectnuxt')
            .end();
    },

    'Test show start of matching flow' : function (browser) {
        browser
            .url(host)
            .waitForElementVisible('body')
            .assert.containsText('h1.subtitle', 'What is it about ...')
            .assert.containsText('div.buttons > button:nth-of-type(1)', 'Object')
            .assert.containsText('div.buttons > button:nth-of-type(2)', 'Person')
            .assert.containsText('div.buttons > button:nth-of-type(3)', 'Contract')
            .assert.containsText('div.buttons > button:nth-of-type(4)', 'Employment')
            .end();
    },


    'Test clicking through matchingflow' : function (browser) {
        //add matchingflow test
        if(matchingflow_test == 'true') {
            browser
                .url(host)
                .waitForElementVisible('body')
                .assert.containsText('h1.subtitle:nth-of-type(1)', 'What is it about ...')
                .assert.containsText('div.buttons:nth-of-type(1) > button:nth-of-type(1)', 'Object')
                .click('section.section:nth-of-type(1) div.buttons > button:nth-of-type(1)')
                .waitForElementVisible('section.section:nth-of-type(2) div.buttons', 1000)
                .assert.containsText('section.section:nth-of-type(2) div.buttons > button:nth-of-type(1)', 'Property')
                .assert.containsText('section.section:nth-of-type(2) div.buttons > button:nth-of-type(2)', 'Vehicle')
                .assert.containsText('section.section:nth-of-type(2) div.buttons > button:nth-of-type(3)', 'Demages')
            // click on Kündigung
                .click('section.section:nth-of-type(2) div.buttons > button:nth-of-type(2)')
                .assert.containsText('section.section:nth-of-type(3) div.buttons > button:nth-of-type(1)', 'Traffic Law')
                .assert.containsText('section.section:nth-of-type(3) div.buttons > button:nth-of-type(2)', 'Criminal Law')
                .assert.containsText('section.section:nth-of-type(3) div.buttons > button:nth-of-type(3)', 'Demages Law')
                .click('section.section:nth-of-type(3) div.buttons > button:nth-of-type(2)')
                .waitForElementVisible('h1', 1000)
                .assert.containsText('h1', 'We found the following lawyer associatet with your case')
                .end();
        }
    }

}
