const webpack = require("webpack");
require('dotenv').config()

module.exports = {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700\" rel=\"stylesheet"}
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */

  css: [
      '~/assets/main.css',
      '~/node_modules/argon-design-system-free/assets/vendor/font-awesome/css/font-awesome.min.css',
      '~/node_modules/argon-design-system-free/assets/scss/argon.scss',
      '@fortawesome/fontawesome-svg-core/styles.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
      "~/plugins/bootstrap.js",
      "~/plugins/vue-notifications.js",
      "~/plugins/vuelidate.js",
      "~/plugins/argon.js",
      "~/plugins/fontawesome.js",
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [

  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/dotenv'
  ],

  axios: {
      baseURL: process.env.BACKEND_API
  },

  auth: {
      redirect: {
          home: '/dashboard/profile'
      },
      strategies: {
          local: {
              endpoints: {
                  login: { url: 'user/lawyer/login', method: 'post', propertyName: 'token' },
                  user:  { url: 'user/lawyer/profile', method: 'get', propertyName: false },
                  logout: false
              }
          }
      }
  },
  /*
  ** Build configuration
  */
  build: {
    vendor: [
        "jquery",
        "bootstrap"
    ],
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery"
      })
    ],

    /*
    ** You can extend webpack config here
    */
    extend (config, {isDev, isClient}) {
    }
  }
}
