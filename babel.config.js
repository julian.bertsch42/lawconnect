module.exports = {
  env: {
    test: {
      "plugins": ["transform-es2015-modules-commonjs"],
      presets: [
        [
          "@babel/env",
          {
            targets: {
              node: 11
            }
          }
        ]
      ]
    }
  }
};
