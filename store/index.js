
const cookieparser = process.server ? require('cookieparser') : undefined

export const state = () => {
    return {
        auth: null,
        counter: 0
    }
}
export const mutations = {
    increment(state) {
        state.counter++
    }
}

export const getters = {
    isAuthenticated(state) {
        return state.auth.loggedIn
    },

    loggedInUser(state) {
        return state.auth.user
    }
}
