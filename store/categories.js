export const state = () => ({
    list: [
        {
            name: 'Allgemeines Vertragsrecht',
        },
        {
            name: 'Baurecht & Architekturrecht',
        },
        {
            name: 'Grundstücksrecht & Immobilienrecht',
        },
        {
            name: 'Handelsrecht & Gesellschaftsrecht',
        },
        {
            name: 'Kaufrecht',
        },
        {
            name: 'Mietrecht & Wohnungseigentumsrecht',
        },
        {
            name: 'Ordnungswidrigkeitsrecht',
        },
        {
            name: 'Schadensersatzrecht & Schmerzensgeldrecht',
        },
        {
            name: 'Steuerrecht',
        },
        {
            name: 'Strafrecht',
        },
        {
            name: 'Versicherungsrecht',
        },
        {
            name: 'Verkehrsrecht',
        },
        {
            name: 'Wirtschaftsrecht',
        },
        {
            name: 'Allgemeines Vertragsrecht',
        },
        {
            name: 'Zivilrecht',
        }

    ],
})