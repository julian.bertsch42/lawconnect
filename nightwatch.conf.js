module.exports = {
    "src_folders": [
        "test/e2e/"// Where the tests are located
    ],
    "output_folder": "./output/", // reports from nightwatch
    "desiredCapabilities" : {
        "browserName" : "chrome",
        "javascriptEnabled" : true,
        "acceptSslCerts" : true,
        "chromeOptions" : {
            //"args" : ['--no-sandbox']
            "args" : ['--headless', '--no-sandbox', '--disable-gpu']
        }
    },
    "webdriver": {
            "start_process": true,
            "server_path": "node_modules/.bin/chromedriver",
            "cli_args": [
                "--verbose",
                "--port=9515"
            ],
            "port": 9515
        },

        "test_settings" : {
            "default" : {
                "desiredCapabilities" : {
                    "browserName" : "chrome"
                }
            }
        }
}